<?php
function _orderHandle($parameter, $method, $isQfPay = 0) {
	db('Aaaa')->insert(['space'=>'actstar', 'module'=>'orderHandle', 'flag'=>'start', 'content'=>'000']);

	//获取报名记录信息
	$signupDao = model('actstar_manage/Signup');
	$signupInfo = $signupDao->getInfoByOrderNo($parameter['out_trade_no']);

	if ($signupInfo['pay_status'] == 1) {
		$nowTime = time();
		//报名记录信息更新
		$data = array(
			'payment_trade_no'      => (string) $parameter['trade_no'],
			'payment_trade_status'  => (string) $parameter['trade_status'],
			'payment_buyer_email'   => (string) $parameter['buyer_email'],
			'payment_notify_time'   => (string) $parameter['notify_time'],
			'pay_method'            => $method,
			'pay_time'              => $nowTime,
			'signup_status'         => 2, //报名状态
			'pay_status'          	=> 2, //支付状态
		);
		$signupDao->baseUpdateData($signupInfo['id'], $data);
	}

	return array($signupInfo['id']);
}