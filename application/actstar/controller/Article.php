<?php
/**
 * @Author: lianlianzan(13040@qq.com)
 * @Date:   2015-10-28 16:40:00
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-17 12:53:03
 */

namespace app\actstar\controller;

class Article extends ActstarBase {

	function initialize() {
		parent::initialize();

		$this->articleDao = model('actstar_manage/Article');
		
		//缓存相关,生成缓存,获取文章分类列表
		$sys_identify = 'articleCategory';
		$cacheData = cache($sys_identify);
		if (empty($cacheData)) {
			$categoryDao = model('actstar_manage/ArticleCategory');
			list($categoryList) = $categoryDao->search(['status'=>1], '');
			cache($sys_identify, $categoryList, 3600);
		} else {
			$categoryList = $cacheData;
		}
		$this->assign('categoryList', $categoryList);
		
		//底部菜单标示
		$this->assign('bottomFlag', 'article');
	}

	public function index() {
		//获取文章列表
		$map = $parameter = array();
		$map['status'] = 1;
		
		$cid = input('param.cid', '', '', 'intval');
		if ($cid) {
			$map['cid'] = $cid;
			$cacheData = cache('articleCategory');
			$categoryInfo = $cacheData[$cid];
			$headTitle = $categoryInfo['name'];
		} else {
			$headTitle = '文章管理';
		}
		$this->assign("cid", $cid);
		
		$keyword = input('param.keyword', '', '', 'pwEscape');
		if ($keyword) {
			$map['title'] = array('like', '%'.$keyword.'%');
		}

		$count = $this->articleDao->countSearch($map);
		$Page = new \org\util\PageBootstrap($count, config('PER_PAGE'), $parameter);
		$pageShow = $Page->show();
		list($articleList) = $this->articleDao->search($map, $Page->getLimit());
		$this->assign("articleList", $articleList);

		//设置seo
    	$this->setSeoFrontNew($headTitle);
        
		return $this->fetch();
	}

	//http://www2.kszhuangxiu.com/applet/article/detail?arid=11
	public function detail() {
		$arid = input('param.arid', '', '', 'intval');
		if (!$arid) {
			$this->jsonResult(5, '非法文章ID');
		}

		//获取文章信息
		$articleInfo = $this->articleDao->getInfo($arid);
		//print_r($articleInfo);
		$articleInfo['autotime'] = getAutoTime($articleInfo['create_time']);

		//点击数更新
		$this->articleDao->updateHits($articleInfo);
		$this->assign("articleInfo", $articleInfo);

		//设置seo
        $this->setSeoFrontNew($articleInfo['title']);
        
		//=====获取评论列表
// 		$map = $parameter = array();
// 		$map['arid'] = $arid;
// 		$count = $this->articleCommentDao->countSearch($map);
// 		$Page = new \org\util\PageBootstrap($count, config('PER_PAGE'), $parameter);
// 		$pageShow = $Page->show();
// 		list($commentList) = $this->articleCommentDao->search($map, $Page->getLimit());
// 		//组装数据
// 		$retCommentList = array();
// 		foreach ($commentList as $key => $value) {
// 			$tmpArr = array();
// 			$tmpArr['id'] = $value['id'];
// 			$tmpArr['username'] = $value['username'];
// 			$tmpArr['avatar'] = $value['avatar'];
// 			$tmpArr['content'] = $value['content'];
// 			$tmpArr['autotime'] = getAutoTime($value['create_time']);
// 			$retCommentList[] = $tmpArr;
// 		}
		//=====相关文章(调取同分类的文章)
// 		list($relateArticleList) = $this->articleDao->search(['cid'=>$articleInfo['cid']], 'limit 0,5');
// 		//组装数据
// 		$retRelateArticleList = array();
// 		foreach ($relateArticleList as $key => $value) {
// 			$tmpArr = array();
// 			$tmpArr['id'] = $value['id'];
// 			$tmpArr['title'] = $value['title'];
// 			$tmpArr['picurl'] = $value['picurl'];
// 			$tmpArr['cate_name'] = $value['cate_name'];
// 			$tmpArr['autotime'] = getAutoTime($value['create_time']);
// 			$retRelateArticleList[] = $tmpArr;
// 		}
		//=====分享相关
// 		$shareInfo = array(
// 			'title' 	=> $articleInfo['title'],
// 			'desc'		=> '',
// 			'link'		=> config('system.site_https_domain').'/h5/#/pages/article/articleDetail?arid='.$arid,
// 			'icon'		=> $articleInfo['picurl'],
// 		);
		return $this->fetch();
	}

}