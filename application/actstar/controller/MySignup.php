<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2018-03-26 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-17 13:50:01
 */

namespace app\actstar\controller;

class MySignup extends ActstarBase {

	function initialize() {
		parent::initialize();
		$this->signupDao = model('actstar_manage/Signup');
		$this->activeDao = model('actstar_manage/Active');
	}

	public function index() {
		$this->checkIsLoginWap(); //提示用户登陆状态

		//获取报名列表
		$map = $parameter = array();
		$map['del_status'] = 1;
		$map['uid'] = $this->moonuid;
		$count = $this->signupDao->countSearch($map);
		$Page = new \org\util\PageBootstrap($count, config('PER_PAGE'), $parameter);
		$pageShow = $Page->show();
		list($signupList, $signupIds, $kids) = $this->signupDao->search($map, $Page->getLimit());
		$this->assign("count", $count);
		$this->assign('pageShow', $pageShow);
		$this->assign("recordList", $recordList);

		//获取活动列表
		list($activeList) = $this->activeDao->getListByIds($kids);
		$this->assign("activeList", $activeList);

		//设置seo
		$this->setSeoFrontNew('我的报名');
		
		return $this->fetch();
	}

	public function detail() {
		$signupId = input('param.signupId', '', 'intval');
		if (!$signupId) {
			$this->wapError('非法ID');
		}
		
		//获取报名信息
		$signupInfo = $this->signupDao->getInfo($signupId);
		if (!$signupInfo) {
			$this->wapError('非法报名信息');
		}

		//获取活动信息
		$activeInfo = $this->activeDao->getInfo($signupInfo['kid']);
		$this->assign("activeInfo", $activeInfo);
		
		//组装报名数据
		foreach ($activeInfo['leavemess_fields_arr'] as $key => $value) {
			if ($value['issys'] == 1) {
				if ($value['flag'] == 'personsex') {
					$signupInfo[$value['flag']] = $signupInfo['personsex_cn'];
				}
			} else {
				if ($value['type'] == 'checkbox') {
					$newValue = getLeavemessValue($value['type'], $value['config'], $signupInfo[$value['flag']]);
					$signupInfo[$value['flag']] = $newValue;
				}
			}
		}
		$this->assign("signupId", $signupId);
		$this->assign("signupInfo", $signupInfo);
		
		//核销二维码地址
		$verifyUrl = config('system.site_share_domain').'/actstar/myWriteoff/ewmWriteoff/signupId/'.$signupId;
		$this->assign("verifyUrl", $verifyUrl);

		//设置seo
		$this->setSeoOrigin('报名详情');
		
		return $this->fetch();
	}

	//报名取消
	public function doCancel() {
		$signupId = input('param.signupId', '', '', 'intval');
		if (!$signupId) {
			$this->error('非法ID');
		}

		//获取报名记录信息
		$signupInfo = $this->signupDao->getInfo($signupId);
		if (!$signupInfo) {
			$this->error('非法报名记录信息');
		}

		if ($signupInfo['uid'] != $this->moonuid) {
			$this->error('不是您的报名记录，无法取消');
		}

		//获取活动信息
		$activeInfo = $this->activeDao->getInfo($signupInfo['kid']);
		if (!$activeInfo['is_allow_cancel']) {
			$this->error('该活动不允许取消报名');
		}

		//修改报名记录状态
		$data = [
			'del_status'	=> 2, //删除状态
			'signup_status'	=> 8, //报名状态
		];
		$this->signupDao->baseUpdateData($signupId, $data);

		$this->success('报名取消成功');
	}

}