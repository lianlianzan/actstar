<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2018-03-26 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-17 12:53:03
 */

namespace app\actstar\controller;

class Active extends ActstarBase {

	function initialize() {
		parent::initialize();

		$this->activeDao = model('actstar_manage/Active');
		$this->activePictureDao = model('actstar_manage/ActivePicture');
		$this->activeMeetingDao = model('actstar_manage/ActiveMeeting');
		$this->signupDao = model('actstar_manage/Signup');

		//缓存相关,生成缓存,获取活动分类列表
		$sys_identify = 'activeCategory';
		$cacheData = cache($sys_identify);
		if (empty($cacheData)) {
			$categoryDao = model('actstar_manage/ActiveCategory');
			list($categoryList) = $categoryDao->search(['status'=>1], '');
			cache($sys_identify, $categoryList, 3600);
		} else {
			$categoryList = $cacheData;
		}
		$this->assign('categoryList', $categoryList);
		
		//底部菜单标示
		$this->assign('bottomFlag', 'active');
	}

	public function index() {
		//获取活动列表
		$map = array(
			'status'   => 1,
		);

		$cid = input('param.cid', '', '', 'intval');
		if ($cid) {
			$map['cid'] = $cid;
			$cacheData = cache('activeCategory');
			$categoryInfo = $cacheData[$cid];
			$headTitle = $categoryInfo['name'];
		} else {
			$headTitle = '活动管理';
		}
		$this->assign("cid", $cid);

		$keyword = input('param.keyword', '', '', 'pwEscape');
		if ($keyword) {
			$map['name'] = array('like', '%'.$keyword.'%');
		}
		
		$count = $this->activeDao->countSearch($map);
		$Page = new \org\util\PageBootstrap($count, config('PER_PAGE'), $parameter);
		$pageShow = $Page->show();
		list($activeList) = $this->activeDao->search($map, $Page->getLimit());
		//print_r($activeList);exit;
		$this->assign('count', $count);
		$this->assign('pageShow', $pageShow);
		$this->assign('activeList', $activeList);

		//设置seo
    	$this->setSeoFrontNew($headTitle);
        
		return $this->fetch();
	}
	
	public function detail() {
		$kid = input('param.kid', '', '', 'intval');
		if (!$kid) {
			$this->wapError("活动ID不存在");
		}
		//获取活动信息
		$activeInfo = $this->activeDao->getInfo($kid);
		//print_r($activeInfo);
		//print_r($activeInfo['leavemess_fields_arr']);
		//验证数据
		if (!$this->activeDao->verifyData($activeInfo, $this->ts)) {
			$this->wapError($this->activeDao->getError());
		}
		//点击数更新
		$this->activeDao->updateHits($activeInfo);
		
		//获取奖品信息
		if ($activeInfo['lottery_status'] == 1) {
			$prizeDao = model('actstar_manage/Lotteryprize');
			list($prizeList) = $prizeDao->search(['kid'=>$kid], '');
			$this->assign('prizeList', $prizeList);
		}
		
		//====获取活动状态
		$statusFlag = 1; //活动进行
		//活动尚未开始
		if ($activeInfo['start_time'] && $this->ts < $activeInfo['start_time']) {
			$statusFlag = 2;
		}
		//活动已经结束
		if ($activeInfo['end_time'] && $this->ts > $activeInfo['end_time']) {
			$statusFlag = 3;
		}
		$this->assign('statusFlag', $statusFlag);

		//获取活动图片列表
		list($pictureList) = $this->activePictureDao->getListByKid($kid);
		//print_r($pictureList);
		$this->assign('pictureList', $pictureList);

		$this->assign('kid', $kid);
		$this->assign('activeInfo', $activeInfo);
		
		if ($activeInfo['is_meeting_open']) {
			//获取场次列表
			list($meetingList) = $this->activeMeetingDao->getListByKid($kid);
			//print_r($meetingList);
			$this->assign('meetingList', $meetingList);
		}
		
		//获取最近报名记录
		$sys_identify = 'actstar_signup_'.$kid;
		$cacheData = cache($sys_identify);
		if (empty($cacheData)) {
			//获取中奖记录列表
			$Page = new \org\util\PageWeui($count, 100, $parameter);
			list($signupList) = $this->signupDao->search(['kid'=>$kid], $Page->getLimit());
			cache($sys_identify, $signupList, 3600);
		} else {
			$recordList = $cacheData;
		}
		$this->assign('recordList', $recordList);
		
		//获取我的报名记录
		if ($this->moonuid) {
			list($mySignupList) = $this->signupDao->search(['kid'=>$kid, 'uid'=>$this->moonuid], '');
			$this->assign('mySignupList', $mySignupList);
		}
		
		//=====收藏和浏览记录等
		$resourceId = $kid;
		$browseTitle = $activeInfo['title'];
		$browseDescrip = '';
		//获取用户收藏
		$collectClass = 'iconfontzz icon-favor';
		if ($this->moonuid) {
			$collectDao = model('actstar_manage/ActiveCollect');
			$collectInfo = $collectDao->getByUidAndKid($this->moonuid, $resourceId);
			if ($collectInfo) {
				$collectClass = 'iconfontzz icon-favor_fill';
			}
		}
		$this->assign('collectClass', $collectClass);

		//更新浏览记录
		if ($this->moonuid) {
			$browseDao = model('actstar_manage/ActiveBrowse');
			$browseInfo = $browseDao->getByUidAndKid($this->moonuid, $resourceId);
			$data = [
				'uid' => $this->moonuid,
				'kid' => $resourceId,
				'active_title' => $browseTitle,
				'descrip' => $browseDescrip,
				'create_time' => $this->ts
			];
			if ($browseInfo) {
				$browseDao->baseUpdateData($browseInfo['id'], $data);
			} else {
				$browseDao->baseAddData($data);
			}
		}
		//=====收藏和浏览记录等 END
		
		//设置seo
		$this->setSeoOrigin($activeInfo['title']);

		//分享相关
		$_share = array();
		$_share['title'] = $activeInfo['share_title'] ? $activeInfo['share_title'] : $activeInfo['name'];
		$_share['desc'] = $activeInfo['share_desc'] ? $activeInfo['share_desc'] : $activeInfo['name'];
		$_share['link'] = $activeInfo['share_url'] ? $activeInfo['share_url'] : config('system.site_share_domain')."/actstar/active/detail/kid/".$kid;
		$_share['imgUrl'] = $activeInfo['picurl'] ? $activeInfo['picurl'] : config('app.ftp_web').$configInfo['proj_logo'];
		$this->assign('_share', $_share);

		//return $this->fetch();
		return $this->fetch('active_detail');
	}
	
	public function getEvaluation() {
		$evaluationDao = model('mobmall_manage/Evaluation');
		$evaluationPictureDao = model('mobmall_manage/EvaluationPicture');

		$kid = input('param.kid', '', '', 'intval');

		//获取活动信息
		$activeInfo = $this->activeDao->getInfo($kid);
		$this->assign("kid", $kid);
		$this->assign("activeInfo", $activeInfo);

		//获取评价列表
		$map = [
			'kid'		=> $kid,
			'status'	=> 2
		];
		$count = $evaluationDao->countSearch($map);
		$Page = new \org\util\PageWeui($count, config('PER_PAGE'), $parameter);
		$pageShow = $Page->show();
		list($evaluationList, $eids, $uids) = $evaluationDao->search($map, $Page->getLimit());
		$this->assign("count", $count);
		$this->assign('pageShow', $pageShow);
		$this->assign("evaluationList", $evaluationList);

		//获取评价图片的组
		list($evaluationPictureGroup) = $evaluationPictureDao->getGroupByEids($eids);
		$this->assign("evaluationPictureGroup", $evaluationPictureGroup);

		//获取用户列表
		list($userList) = $this->userDao->getListByIds($uids);
		$this->assign("userList", $userList);

		echo $this->fetch()->getContent();
	}
	
}