<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2018-03-26 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-17 12:53:03
 */

namespace app\actstar\controller;

class Active123 extends Active {

	function initialize() {
		parent::initialize();
	}

	//https://wx.ksmeishi.cn/actstar/active123/detail10/kid/13.html
	public function detail10() {
		return $this->detail();
	}

	public function detail11() {
		return $this->detail();
	}

	public function detail12() {
		return $this->detail();
	}

	public function detail13() {
		return $this->detail();
	}

	public function detail14() {
		return $this->detail();
	}

	public function detail15() {
		return $this->detail();
	}

	public function detail16() {
		return $this->detail();
	}

	public function detail17() {
		return $this->detail();
	}

	public function detail18() {
		return $this->detail();
	}

	public function detail19() {
		return $this->detail();
	}
		
}