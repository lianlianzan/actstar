<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2015-09-16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-19 10:03:49
 */

namespace app\actstar\controller;
use think\Image;
use think\Db;

class MyWriteoff extends ActstarBase {

	function initialize() {
		parent::initialize();

		$this->activeDao = model('actstar_manage/Active');
		$this->signupDao = model('actstar_manage/Signup');
	}

	public function setWriteoff() {
		$id = input('param.id', '', '', 'intval');
		$this->assign('id', $id);

		//核销二维码地址
		$writeoffUrl = config('system.site_share_domain').'/actstar/myWriteoff/ewmWriteoff/signupId/'.$id;
		$this->assign("writeoffUrl", $writeoffUrl);

		echo $this->fetch()->getContent();
	}

	public function doSetWriteoff() {
		$id = input('post.id', '', '', 'intval');
		$verify_code = input('post.verify_code', '', '', 'pwEscape');
		$remark = input('post.remark', '', '', 'pwEscape');
		if (!$verify_code) {
			$this->error('请输入领奖暗号');
		}
		//获取记录信息
		$signupInfo = $this->signupDao->getInfo($id);
		if (!$signupInfo) {
			$this->wapError('该记录不存在');
		}
		$kid = $signupInfo['kid'];
		//获取活动信息
		$activeInfo = $this->activeDao->getInfo($kid);
		$this->checkIsLoginAjax(); //提示用户登陆状态
		if (procLock('record_writeoff', $this->moonuid)) { //物理锁
			if ($verify_code != $activeInfo['verify_code']) {
				procUnLock('record_writeoff', $this->moonuid); //物理锁
				$this->error('领奖暗号输入有误');
			}
			$data = array(
				'writeoff_status'	=> 2, //核销状态
				'writeoff_uid'		=> $this->moonuid, //核销用户uid
				'writeoff_username'	=> $this->moonname, //核销用户名
				'writeoff_time'		=> $this->ts, //核销时间
				'writeoff_remark'	=> $remark,
			);
			$this->signupDao->baseUpdateData($id, $data);
			procUnLock('record_writeoff', $this->moonuid); //物理锁
			$this->success('该记录核销成功');
		} else {
			procUnLock('record_writeoff', $this->moonuid); //物理锁
			$this->error('请重新操作');
		}
	}

	//http://www2.kszhuangxiu.com/actstar/myWriteoff/ewmWriteoff/signupId/77005.html
	public function ewmWriteoff() {
		$signupId = input('param.signupId', '', '', 'intval');
		if (!$signupId) {
			$this->wapError('非法记录ID');
		}
		//获取报名记录信息
		$signupInfo = $this->signupDao->getInfo($signupId);
		if (!$signupInfo) {
			$this->wapError('该记录不存在');
		}
		$this->assign('signupId', $signupId);
		$this->assign('signupInfo', $signupInfo);

		//获取活动信息
		$activeInfo = $this->activeDao->getInfo($signupInfo['kid']);
		$this->assign("activeInfo", $activeInfo);
		
		//设置seo
		$this->setSeoFrontNew('记录核销页面');

		return $this->fetch();
	}

	public function doEwmWriteoff() {
		$signupId = input('post.signupId', '', '', 'intval');
		$remark = input('post.remark', '', '', 'pwEscape');
		if (!$signupId) {
			$this->error('非法记录ID');
		}
		//获取报名记录信息
		$signupInfo = $this->signupDao->getInfo($signupId);
		if ($signupInfo['writeoff_status'] == 2) {
			procUnLock('record_writeoff', $this->moonuid); //物理锁
			$this->error('订单已核销，请勿重复核销');
		}
		$kid = $signupInfo['kid'];
		//获取活动信息
		$activeInfo = $this->activeDao->getInfo($kid);
		$this->checkIsLoginAjax(); //提示用户登陆状态
		//验证是否管理员
		if ($this->moondb['is_writeoff'] == 0) {
			procUnLock('record_writeoff', $this->moonuid); //物理锁
			$this->error('您不是管理员，无法核销');
		}
		if (procLock('record_writeoff', $this->moonuid)) { //物理锁
			//修改记录状态
			$data = array(
				'signup_status'		=> 5, //报名状态
				'writeoff_status'	=> 2, //核销状态
				'writeoff_uid'		=> $this->moonuid, //核销用户uid
				'writeoff_name'		=> $this->moonname, //核销用户名
				'writeoff_time'		=> $this->ts, //核销时间
				'writeoff_remark'	=> $remark //核销备注
			);
			$this->signupDao->baseUpdateData($signupId, $data);
			procUnLock('record_writeoff', $this->moonuid); //物理锁
			$this->success('该记录核销成功', '', ['jump_type'=>'reload']);
		} else {
			procUnLock('record_writeoff', $this->moonuid); //物理锁
			$this->error('请重新操作');
		}
	}

	public function doShare() {
		db('Aaaa')->insert(['space'=>'dzp', 'module'=>'doShare', 'flag'=>'start', 'content'=>'start']);
		$kid = input('post.kid', '', '', 'intval');

		//获取活动信息
		$activeInfo = $this->activeDao->getInfo($kid);

		$this->checkIsLoginAjax(); //提示用户登陆状态
		if ($this->moondb['is_share'] == 0) {
			$data = array(
				'is_share'  => 1,
			);
			$this->userDao->baseUpdateData($this->moonuid, $data);
		}
		$this->activeDao->where(['id'=>$kid])->setInc('share_num', 1);
		if ($activeInfo['limit_day_share_num']) {
			//获取今日分享记录
			$checkShareInfo = $this->shareDao->getUserTdCountByKidAndUid($kid, $this->moonuid, $this->tdtime);
			if (!$checkShareInfo) {
				$data = array(
					'kid'			=> $kid,
					'uid'			=> $this->moonuid,
					'pwuid'			=> $this->moonpwuid,
					'openid'		=> $this->moondb['openid'],
					'unionid'		=> $this->moondb['unionid'],
					'username'		=> $this->moonname,
					'create_time'   => $this->ts
				);
				$this->shareDao->baseAddData($data);
				$this->success('恭喜您转发成功，获得'.$activeInfo['limit_day_share_num'].'次抽奖机会');
			}
			$this->success('感谢您的参与，您今天已分享');
		}
		$this->success('您已成功转发，快去抽奖吧！');
	}

	function showPoster() {
		$kid = input('param.kid', '', '', 'intval');
		$this->assign('kid', $kid);

		echo $this->fetch()->getContent();
	}

	//http://www.kszhuangxiu.com/market/dzp/scimg.html?name=%E8%B5%B5%E5%AD%90%E9%BE%99
	public function scimg() {
		$kid = input('param.kid', '', '', 'intval');

		//获取活动信息
		$activeInfo = $this->activeDao->getInfo($kid);

		// if ($uid) {
		// 	$userInfo = $this->userDao->getInfo($uid); //根据uid获取用户信息
		// } else {
		// 	$userInfo = array(
		// 		'from'		=> 'app-none',
		// 		'uid'		=> 0,
		// 		'pwuid'		=> 0,
		// 		'username'	=> '游客',
		// 		'avatar'	=> 'http://www.kszhuangxiu.com/tplnew/public/images/avatar/none.gif',
		// 		'mobile'	=> '',
		// 	);
		// }

		$localImg = PUBLIC_PATH.'data/dzp/active_'.$kid.'_origin.jpg';
		$realImg = PUBLIC_PATH.'data/dzp/a_'.$kid.'_'.$rid.'.jpg';
		$qrcodeImg = PUBLIC_PATH.'data/dzp/qrcode_'.$kid.'.jpg';

		//创建本地存储目录
		createSavePath(PUBLIC_PATH.'data/dzp/');

		//先把图片下载到本地
		$curl = curl_init($activeInfo['poster_img']);
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
		$imageData = curl_exec($curl);
		curl_close($curl);
		$tp = fopen($localImg, "w");
		fwrite($tp, $imageData);
		fclose($tp);

		//打开图片
		$image = Image::open($localImg);

		//添加姓名(文字)
		//$image->text($signupInfo['realname'], CMF_ROOT.'extend/fonts/simhei.ttf', $activeInfo['name_font'], $activeInfo['name_color'], 1, array($activeInfo['name_left'], $activeInfo['name_top']), 0);

		//=====添加二维码(当场生成链接地址)
		require_once EXTEND_PATH.'phpqrcode/phpqrcode.php';
		$url = config('system.site_share_domain').url('market/dzp/detail/kid', ['kid'=>$kid, 'uid'=>$this->moonuid]);
		// 纠错级别：L、M、Q、H
		$level = 'L';
		// 点的大小：1到10,用于手机端4就可以了
		$size = $activeInfo['ewm_size'] ? $activeInfo['ewm_size'] : 4;

		$padding = 2; //二维码边框的间距，默认2
		// 下面注释了把二维码图片保存到本地的代码,如果要保存图片,用$fileName替换第二个参数false
		\QRcode::png($url, $qrcodeImg, $level, $size, $padding);

		$image->water($qrcodeImg, array($activeInfo['ewm_left'], $activeInfo['ewm_top']));
		//=====添加二维码

		$image->save($realImg);
		//echo $realImg;exit;

		//后续处理
		//@unlink($avatarimg);

		ob_end_clean();
		header('Content-Type: image/jpeg');
		echo file_get_contents($realImg);
	}

}