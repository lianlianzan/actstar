<?php
return [
	[
		'id' => 1,
		'name' => '未来之星系统',
		'icon' => '',
		'is_sub' => 0,
		'subs' => [
			[
				'name' => '首页',
				'icon' => '',
				'url' => '/actstar_manage/index/index',
				'sideFlag' => 'index',
			],
			[
				'name' => '配置',
				'icon' => '',
				'url' => '/actstar_manage/config/index',
				'sideFlag' => 'config',
			],
			[
				'name' => '图片轮播管理',
				'icon' => '',
				'url' => '/actstar_manage/slide/index',
				'sideFlag' => 'slide',
			],
			[
				'name' => '导航管理',
				'icon' => '',
				'url' => '/actstar_manage/navigation/index',
				'sideFlag' => 'navigation',
			],
			[
				'name' => '公告管理',
				'icon' => '',
				'url' => '/actstar_manage/announce/index',
				'sideFlag' => 'announce',
			],
			[
				'name' => '弹出公告牌',
				'icon' => '',
				'url' => '/actstar_manage/bulletin/index',
				'sideFlag' => 'bulletin',
			],
			[
				'name' => '自定义模板',
				'icon' => '',
				'url' => '/actstar_manage/wapcustom/index',
				'sideFlag' => 'wapcustom',
			],
			[
				'name' => '用户管理',
				'icon' => '',
				'url' => '/actstar_manage/user/index',
				'sideFlag' => 'user',
			],
		],
	],
	[
		'id' => 2,
		'name' => '内容管理',
		'icon' => '',
		'is_sub' => 0,
		'subs' => [
			[
				'name' => '活动管理',
				'icon' => '',
				'url' => '/actstar_manage/active/index',
				'sideFlag' => 'active',
			],
			[
				'name' => '活动分类',
				'icon' => '',
				'url' => '/actstar_manage/activeCategory/index',
				'sideFlag' => 'activeCategory',
			],
			[
				'name' => '文章管理',
				'icon' => '',
				'url' => '/actstar_manage/article/index',
				'sideFlag' => 'article',
			],
			[
				'name' => '文章分类',
				'icon' => '',
				'url' => '/actstar_manage/articleCategory/index',
				'sideFlag' => 'articleCategory',
			],
			[
				'name' => '报名记录管理',
				'icon' => '',
				'url' => '/actstar_manage/signup/index',
				'sideFlag' => 'record',
			],
			[
				'name' => '报名退款管理',
				'icon' => '',
				'url' => '/actstar_manage/signupRefund/index',
				'sideFlag' => 'orderRefund',
			],
			[
				'name' => '评价管理',
				'icon' => '',
				'url' => '/actstar_manage/evaluation/index',
				'sideFlag' => 'evaluation',
			],
		],
	],
];