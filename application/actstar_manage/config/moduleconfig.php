<?php
//配置文件
return [
	'sys_identify'	=> 'actstar',
	'sys_name'	=> '活动之星',

	//展示形式
	'exhibition_config' => [
		'1'	=> '纯文字',
		'2'	=> '左图片右文字',
		'3'	=> '左文字右图片',
		'4'	=> '上图片下文字',
		'5'	=> '上三图片下文字',
		'6'	=> '上视频下文字',
	],
	
	//删除状态
	'del_status_config' => [
		'1'		=> '正常',
		'2'		=> '<font color="red">已失效</font>',
	],
	
	//报名状态
	'signup_status_config' => [
		'1'	=> '待付款',
		'2'	=> '已报名',
		'5'	=> '已核销',
		'6'	=> '用户取消',
		'7'	=> '已关闭',
	],

	//支付状态
	'pay_status_config' => [
		'1'		=> '待付款',
		'2'		=> '已支付',
	],
	
	//=====自定义模板相关
	//模块类别
	'wapcustom_kind_config' => [
		'1'		=> [
			'name'	=> '广告位',
			'flag'	=> 'advert',
		],
		'2'		=> [
			'name'	=> '自定义模板',
			'flag'	=> 'custom',
		],
	],

	//推送方式
	'wapcustom_push_config' => [
		'1'	=> '自动',
		'2'	=> '手动'
	],

	//数据来源
	'wapcustom_source_config' => [
		'1'	=> [
			'id' 	=> 1,
			'flag'	=> 'active',
			'name'	=> '活动',
			'sort'	=> [
				'1'	=> '最新',
				'2'	=> '默认排序',
				'3'	=> '最热',
				'4'	=> '报名最多',
			]
		],
		'2'	=> [
			'id' 	=> 2,
			'flag'	=> 'article',
			'name'	=> '文章',
			'sort'	=> [
				'1'	=> '最新',
				'2'	=> '默认排序',
				'3'	=> '最热',
			]
		],
	],

	//留言字段相关-选项
	'leavemess_option_config' => [
		'1' => array(
			'issys'	=> 1,
			'name'	=> '姓名',
			'desc'	=> '系统预设字段',
			'flag'	=> 'realname',
		),
		'2' => array(
			'issys'	=> 1,
			'name'	=> '手机',
			'desc'	=> '系统预设字段',
			'flag'	=> 'mobile',
		),
		'3' => array(
			'issys'	=> 1,
			'name'	=> '身份证',
			'desc'	=> '系统预设字段',
			'flag'	=> 'idcard',
		),
		'4' => array(
			'issys'	=> 1,
			'name'	=> '地址',
			'desc'	=> '系统预设字段',
			'flag'	=> 'address',
		),
		'5' => array(
			'issys'	=> 1,
			'name'	=> '人数',
			'desc'	=> '系统预设字段',
			'flag'	=> 'personnum',
		),
		'6' => array(
			'issys'	=> 1,
			'name'	=> '性别',
			'desc'	=> '系统预设字段',
			'flag'	=> 'personsex',
		),
		'7' => array(
			'issys'	=> 1,
			'name'	=> '年龄',
			'desc'	=> '系统预设字段',
			'flag'	=> 'personage',
		),
		'8' => array(
			'issys'	=> 1,
			'name'	=> '生日',
			'desc'	=> '系统预设字段',
			'flag'	=> 'birthday',
		),
		'21' => array(
			'name'	=> '扩展字段一',
			'flag'	=> 'expand1',
		),
		'22' => array(
			'name'	=> '扩展字段二',
			'flag'	=> 'expand2',
		),
		'23' => array(
			'name'	=> '扩展字段三',
			'flag'	=> 'expand3',
		),
		'24' => array(
			'name'	=> '扩展字段四',
			'flag'	=> 'expand4',
		),
	],

	//留言字段相关-类型
	'leavemess_type_config' => [
		'text'		=> '文字',
		'textarea'	=> '多行文字',
		'radio'		=> '单选',
		'checkbox'	=> '多选',
		'select'	=> '下拉选择',
		'img'		=> '上传图片',
	],

	//=====页面跳转相关
	//目标页面类型
	'page_type_config' => [
		'1' => [
			'flag' => 'index',
			'name' => '首页',
		],
		'7' => [
			'flag' => 'mine',
			'name' => '个人中心',
		],
		'9' => [
			'flag' => 'link',
			'name' => '外链',
		],
		'21' => [
			'flag' => 'active_list',
			'name' => '活动列表',
		],
		'22' => [
			'flag' => 'active_detail',
			'name' => '活动详情',
		],
		'31' => [
			'flag' => 'article_list',
			'name' => '文章列表',
		],
		'32' => [
			'flag' => 'article_detail',
			'name' => '文章详情',
		],
	],

	//目标页面图标
	'page_icon_config' => [
		'1' => 'footer-home',
		'2' => 'footer-category',
		'3' => 'footer-card',
		'4' => 'footer-user',
		'5' => 'footer-shopcart',
		'6' => 'footer-evaluation',
		'7' => 'footer-shop',
	],

];