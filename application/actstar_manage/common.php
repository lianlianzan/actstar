<?php
/**
 * 处理链接
 */
function dealLinkTwo($page_flag, $page_data='', $page_extra='') {
	if ($page_flag == 'index') {
		$link = url('actstar/index/index');
	} else if ($page_flag == 'active_list') {
		$link = url('actstar/active/index', ['cid'=>$page_data]);
	} else if ($page_flag == 'active_detail') {
		$link = url('actstar/active/detail', ['kid'=>$page_data]);
	} else if ($page_flag == 'article_list') {
		$link = url('actstar/article/index', ['cid'=>$page_data]);
	} else if ($page_flag == 'article_detail') {
		$link = url('actstar/article/detail', ['arid'=>$page_data]);
	} else if ($page_flag == 'mine') {
		$link = url('actstar/mine/index');
	} else if ($page_flag == 'link') {
		$link = $page_extra;
	} else {
		$link = url('actstar/index/index');
	}
	return $link;
}
