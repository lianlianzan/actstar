<?php
/**
 * @Author: lianlianzan(13040@qq.com)
 * @Date:   2018-03-26 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-17 15:01:31
 */

namespace app\actstar_manage\controller;
use app\common\controller\ManageBase;

class Announce extends ManageBase {

	function initialize() {
		parent::initialize();
		$this->saveAndGetConfigIdentify(model('Config'), '公告管理', '', 'announce');

		$this->announceDao = model('Announce');
	}

	function index() {
		//获取列表
		$map = $parameter = array();

		$count = $this->announceDao->countSearch($map);
		$Page = new \org\util\PageBootstrap($count, config('PER_PAGE'), $parameter);
		$pageShow = $Page->show();
		list($list) = $this->announceDao->search($map, $Page->getLimit());
		$this->assign("count", $count);
		$this->assign('pageShow', $pageShow);
		$this->assign("list", $list);

		return $this->fetch();
	}

	public function listorders() {
		$result = parent::listordersOrigin($this->announceDao);
		if ($result !== false) {
			$this->success('排序更新成功');
		} else {
			$this->error('排序更新失败');
		}
	}

	function add() {
		$id = input('param.id', '', '', 'intval');

		if ($id) {
			$info = $this->announceDao->getInfo($id);
		} else {
			$info = [
				'status' => 1
			];
		}
		$this->assign('id', $id);
		$this->assign('info', $info);

		$this->assign('color_config', config('color_config')); //颜色
		$this->assign('page_type_config', config('page_type_config')); //目标页面类型
		return $this->fetch();
	}

	function doAdd() {
		$id = input('post.id', '', '', 'intval');
		$data = input('post.data/a', '', '', 'pwEscape');

		if ($id) {
			$info = $this->announceDao->get($id);
		}

		//验证数据
		//$this->validateData($data, '');

		if ($id) {
			$result = $this->announceDao->baseUpdateData($id, $data);
			if ($result !== false) {
				$this->clearCache(); //清除缓存
				$this->success('保存成功', url('actstar_manage/announce/index'));
			} else {
				$this->error('保存失败'.showDbError($this->announceDao));
			}
		} else {
			$data['create_time'] = $this->ts;
			$result = $this->announceDao->baseAddData($data);
			if ($result !== false) {
				$this->clearCache(); //清除缓存
				$this->success('添加成功', url('actstar_manage/announce/index'));
			} else {
				$this->error('添加失败'.showDbError($this->announceDao));
			}
		}
	}

	function doDelete() {
		$id = input('param.id', '', '', 'intval');

		$result = $this->announceDao->delInfo($id);

		if ($result !== false) {
			$this->clearCache(); //清除缓存
			$this->success('删除成功', url('actstar_manage/announce/index'));
		} else {
			$this->error('删除失败'.showDbError($this->announceDao));
		}
	}

	private function clearCache() {
		$sys_identify = 'actstar_announce';
		cache($sys_identify, NULL); //删除缓存数据
	}
	
}