<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2018-08-18 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-19 14:45:38
 */

namespace app\actstar_manage\controller;
use app\common\controller\ManageBase;

class Evaluation extends ManageBase {

	function initialize() {
		parent::initialize();
		$this->saveAndGetConfigIdentify(model('Config'), '评价管理', '', 'evaluation');

		$this->evaluationDao = model('Evaluation');
		$this->evaluationPictureDao = model('EvaluationPicture');
		$this->productDao = model('Product');
	}

	public function index() {
		//获取评价列表
		list($map, $parameter) = $this->getMap();

		$count = $this->evaluationDao->countSearch($map);
		$Page = new \org\util\PageBootstrap($count, config('PER_PAGE'), $parameter);
		$pageShow = $Page->show();
		list($list, $eids, $pids, $uids) = $this->evaluationDao->search($map, $Page->getLimit());
		//print_r($list);exit;
		$this->assign('count', $count);
		$this->assign('pageShow', $pageShow);
		$this->assign('list', $list);

		//获取评价图片组
		list($pictureGroup) = $this->evaluationPictureDao->getGroupByEids($eids);
		$this->assign('pictureGroup', $pictureGroup);

		//获取商品列表
		list($productList) = $this->productDao->getListByIds($pids);
		$this->assign("productList", $productList);

		return $this->fetch();
	}

	private function getMap() {
		$map = $parameter = array();

		$id = input('param.id', '', '', 'intval');
		if ($id) {
			$map['id'] = $id;
			$parameter['id'] = $id;
		}
		$this->assign('id', $id);

		$order_id = input('param.order_id', '', '', 'intval');
		if ($order_id) {
			$map['order_id'] = $order_id;
			$parameter['order_id'] = $order_id;
		}
		$this->assign('order_id', $order_id);

		$pid = input('param.pid', '', '', 'intval');
		if ($pid) {
			$map['pid'] = $pid;
			$parameter['pid'] = $pid;
		}
		$this->assign('pid', $pid);

		$score = input('param.score', '', '', 'intval');
		if ($score) {
			$map['score'] = $score;
			$parameter['score'] = $score;
		}
		$this->assign('score', $score);

		$content = input('param.content', '', '', 'intval');
		if ($content) {
			$map['content'] = array('like', '%'.$content.'%');
			$parameter['content'] = $content;
		}
		$this->assign('content', $content);

		return array($map, $parameter);
	}

	public function add() {
		$id = input('param.id', '', '', 'intval');

		if ($id) {
			$info = $this->evaluationDao->getInfo($id);
		}
		$this->assign('id', $id);
		$this->assign('info', $info);

		return $this->fetch();
	}

	public function doAdd() {
		$id = input('post.id', '', '', 'intval');
		$data = input('post.data/a', '', '', 'pwEscape');

		if ($id) {
			$info = $this->evaluationDao->get($id);
		}

		if ($id) {
			$result = $this->evaluationDao->baseUpdateData($id, $data);
			if ($result !== false) {
				$this->success('保存成功', url('mobmall_manage/evaluation/index'));
			} else {
				$this->error('保存失败'.showDbError($this->evaluationDao));
			}
		} else {
			$result = $this->evaluationDao->baseAddData($data);
			if ($result !== false) {
				$this->success('添加成功', url('mobmall_manage/evaluation/index'));
			} else {
				$this->error('添加失败'.showDbError($this->evaluationDao));
			}
		}
	}

	public function doDelete() {
		$id = input('param.id', '', '', 'intval');

		//删除对应数据库信息
		$result = $this->evaluationDao->delInfo($id);

		//获取图片列表
		list($pictureList) = $this->evaluationPictureDao->getListByEid($id);

		//删除图片文件
		$ftpConfig = config('FTP_CONFIG');
		$ftp = new \org\util\FtpCmd();
		$ftp->init($ftpConfig['server'], $ftpConfig['port'], $ftpConfig['username'], $ftpConfig['password'], $ftpConfig['dir']);
		foreach ($pictureList as $key => $value) {
			$ftp->delete($value['picurl']);
		}

		if ($result !== false) {
			$this->success('删除成功', url('mobmall_manage/evaluation/index'));
		} else {
			$this->error('删除失败'.showDbError($this->evaluationDao));
		}
	}

}