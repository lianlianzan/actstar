<?php
/**
 * @Author: lianlianzan(13040@qq.com)
 * @Date:   2018-09-11 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-18 10:18:35
 */

namespace app\actstar_manage\controller;
use app\common\controller\ManageBase;

class Bulletin extends ManageBase {

	function initialize() {
		parent::initialize();
		$this->saveAndGetConfigIdentify(model('Config'), '弹出公告牌', '', 'bulletin');

		$this->bulletinDao = model('Bulletin');
	}

	public function index() {
		//获取列表
		$map = $parameter = array();

		$count = $this->bulletinDao->countSearch($map);
		$Page = new \org\util\PageBootstrap($count, config('PER_PAGE'), $parameter);
		$pageShow = $Page->show();
		list($list) = $this->bulletinDao->search($map, $Page->getLimit());
		$this->assign("count", $count);
		$this->assign('pageShow', $pageShow);
		$this->assign("list", $list);

		return $this->fetch();
	}

	public function listorders() {
		$result = parent::listordersOrigin($this->bulletinDao);

		if ($result !== false) {
			$this->success('排序更新成功');
		} else {
			$this->error('排序更新失败'.showDbError($this->bulletinDao));
		}
	}

	public function add() {
		$id = input('param.id', '', '', 'intval');

		if ($id) {
			$info = $this->bulletinDao->getInfo($id);
		} else {
			$info = [
				'status' => 1
			];
		}
		$this->assign('id', $id);
		$this->assign('info', $info);

		$this->assign('page_type_config', config('page_type_config')); //跳转页面类型
		return $this->fetch();
	}

	public function doAdd() {
		$id = input('post.id', '', '', 'intval');
		$data = input('post.data/a', '', '', 'pwEscape');

		if ($id) {
			$info = $this->bulletinDao->get($id);
		}

		//处理编辑器内容
		$data['content'] = ueditorContent($data['content']);

		//验证数据
		//$this->validateData($data, 'ManageGroup.edit');

		if ($id) {
			$result = $this->bulletinDao->baseUpdateData($id, $data);
			if ($result !== false) {
				$this->clearCache(); //清除缓存
				$this->success('保存成功', url('actstar_manage/bulletin/index'));
			} else {
				$this->error('保存失败'.showDbError($this->bulletinDao));
			}
		} else {
			$data['create_time'] = $this->ts;
			$result = $this->bulletinDao->baseAddData($data);
			if ($result !== false) {
				$this->clearCache(); //清除缓存
				$this->success('添加成功', url('actstar_manage/bulletin/index'));
			} else {
				$this->error('添加失败'.showDbError($this->bulletinDao));
			}
		}
	}

	public function doDelete() {
		$id = input('param.id', '', '', 'intval');

		//获取信息(不解析)
		$info = $this->bulletinDao->get($id);

		//删除对应数据库信息
		$result = $this->bulletinDao->delInfo($id);

		//删除图片文件
		$info['picurl'] && $this->delFtpFile($info['picurl']);

		if ($result !== false) {
			$this->clearCache(); //清除缓存
			$this->success('删除成功', url('actstar_manage/bulletin/index'));
		} else {
			$this->error('删除失败'.showDbError($this->bulletinDao));
		}
	}

	public function setDefault() {
		$id = input('param.id', '', '', 'intval');
		if (!$id) {
			$this->error('非法ID');
		}

		//全部置0
		$data = [
			'is_default'	=> 0,
		];
		$this->bulletinDao->where('1=1')->update($data);
		
		//设置默认
		$data = [
			'is_default'	=> 1,
		];
		$result = $this->bulletinDao->baseUpdateData($id, $data);
		
		if ($result !== false) {
			$this->success('设置默认成功', url('actstar_manage/bulletin/index'));
		} else {
			$this->error('设置默认失败'.showDbError($this->bulletinDao));
		}
	}
	
	private function clearCache() {
		$sys_identify = 'actstar_bulletin';
		cache($sys_identify, NULL); //删除缓存数据
	}
	
}