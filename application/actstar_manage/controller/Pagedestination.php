<?php
/**
 * @Author: lianlianzan(13040@qq.com)
 * @Date:   2019-01-10 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-17 14:34:03
 */

namespace app\actstar_manage\controller;
use app\common\controller\ManageBase;

class Pagedestination extends ManageBase {

	function initialize() {
		parent::initialize();
		$this->wapcustomPieceDao = model('WapcustomPiece');
		$this->wapcustomInfopushDao = model('WapcustomInfopush');
	}
	
	/**
	 * 选择目标页面类型
	 */
	public function getPageFlag() {
		$mode = input('param.mode', '', 'pwEscape');
		$this->assign('mode', $mode);

		$selVal = input('param.selVal', '', '', 'intval');

		$page_type_config = config('page_type_config'); //目标页面类型
		$this->assign('page_type_config', $page_type_config);

		$pageTypeInfo = [];
		foreach ($page_type_config as $key => $value) {
			if ($key == $selVal) {
				$pageTypeInfo = $value;
			}
		}
		$this->assign('pageTypeInfo', $pageTypeInfo);

		if ($pageTypeInfo['flag'] == 'active_list') { //活动列表
			//获取商品分类列表
			$categoryDao = model('ActiveCategory');
			list($categoryList) = $categoryDao->search('', '');
			//print_r($categoryList);exit;
			$this->assign('categoryList', $categoryList);
		} else if ($pageTypeInfo['flag'] == 'active_detail') { //活动详情
			//获取商品列表
			$activeDao = model('Active');
			list($activeList) = $activeDao->search('', 'limit 0, 10');
			//print_r($activeList);exit;
			$this->assign('activeList', $activeList);
		} else if ($pageTypeInfo['flag'] == 'article_list') { //文章列表
			//获取文章分类列表
			$articleCategoryDao = model('ArticleCategory');
			list($categoryList) = $articleCategoryDao->search('', '');
			$this->assign('categoryList', $categoryList);
		} else if ($pageTypeInfo['flag'] == 'article_detail') { //文章详情
			//获取文章列表
			$articleDao = model('Article');
			list($articleList) = $articleDao->search('', 'limit 0, 10');
			//print_r($articleList);exit;
			$this->assign('articleList', $articleList);
		} else if ($pageTypeInfo['flag'] == 'link') { //外链
		} else {
			$resultData = [
				'type' => 2,
				'pageTypeInfo' => $pageTypeInfo
			];
			$this->success('', '', $resultData);
		}
		$html = $this->fetch();
		$resultData = [
			'type' 		=> 1,
			'title'		=> $pageTypeInfo['name'],
			'html' 		=> $html
		];
		$this->success('', '', $resultData);
	}

	/**
	 * 主要供wapcustom使用
	 */
	public function pieceForSource($data) {
		$wapcustom_source_config = config('wapcustom_source_config'); //数据来源
		if ($data['data_source'] == '1') { //商品
			if ($data['data_category']) {
				$categoryDao = model('ProductCategory');
				$categoryInfo = $categoryDao->getInfo($data['data_category']);
				if ($categoryInfo['parentid']) {
					$data['cate_is_sub'] = 1;
				} else {
					$data['cate_is_sub'] = 0;
				}
				$data['cate_name'] = $categoryInfo['name'];
			}
			if ($data['data_sort']) {
				$data['sort_name'] = $wapcustom_source_config[$data['data_source']]['sort'][$data['data_sort']];
			}
		} else if ($data['data_source'] == '2') { //文章
			if ($data['data_category']) {
				$categoryDao = model('ArticleCategory');
				$categoryInfo = $categoryDao->getInfo($data['data_category']);
				$data['cate_name'] = $categoryInfo['name'];
			}
			if ($data['data_sort']) {
				$data['sort_name'] = $wapcustom_source_config[$data['data_source']]['sort'][$data['data_sort']];
			}
		}
		return $data;
	}
	
	/**
	 * 根据模块ID获取数据源,主要供wapcustom使用
	 */
	public function getDataSource() {
		$pieceId = input('param.pieceId', '', '', 'intval');
		//获取模块信息
		$pieceInfo = $this->wapcustomPieceDao->getInfo($pieceId);
		$this->assign('pieceInfo', $pieceInfo);
		//数据来源信息
		$sourceFlag = input('param.sourceFlag', '', 'pwEscape');
		$wapcustom_source_config = config('wapcustom_source_config'); //数据来源
		//获取当前数据来源信息
		$sourceInfo = array();
		foreach ($wapcustom_source_config as $key => $value) {
			if ($sourceFlag == $value['flag']) {
				$sourceInfo = $value;
			}
		}
		if ($sourceFlag == 'active') { //活动
			//获取活动分类列表
			$categoryDao = model('ActiveCategory');
			list($categoryList) = $categoryDao->search(['status'=>1], '');
			$this->assign('categoryList', $categoryList);
			$sortList = $sourceInfo['sort'];
			$this->assign('sortList', $sortList);
			$html = $this->fetch('property_custom_active');
			$this->success('', '', $html);
		} else if ($sourceFlag == 'article') { //文章
			//获取文章分类列表
			$categoryDao = model('ArticleCategory');
			list($categoryList) = $categoryDao->search(['status'=>1], '');
			$this->assign('categoryList', $categoryList);
			$sortList = $sourceInfo['sort'];
			$this->assign('sortList', $sortList);
			$html = $this->fetch('property_custom_article');
			$this->success('', '', $html);
		}
	}
	
	/**
	 * 选择icon
	 */
	public function getIcon() {
		$page_icon_config = config('page_icon_config'); //目标页面图标
		$this->assign('page_icon_config', $page_icon_config);

		echo $this->fetch()->getContent();
	}
	
	/**
	 * 获取数据源列表(活动)
	 */
	public function infopushListActive() {

		echo $this->fetch()->getContent();
	}

	/**
	 * 获取数据源列表(文章)
	 */
	public function infopushListArticle() {

		echo $this->fetch()->getContent();
	}
	
}