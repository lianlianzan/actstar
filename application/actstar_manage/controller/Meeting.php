<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2018-07-11 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2020-12-28 13:18:31
 */

namespace app\actstar_manage\controller;
use app\common\controller\ManageBase;

class Meeting extends ManageBase {

	function initialize() {
		parent::initialize();
		$this->saveAndGetConfigIdentify(model('Config'), '场次管理', '', 'meeting');

		$this->activeDao = model('Active');
		$this->activeMeetingDao = model('ActiveMeeting');
	}

	public function index() {
		$map = $parameter = array();

		$kid = input('param.kid', '', '', 'intval');
		if ($kid) {
			$map['kid'] = $kid;
			$parameter['kid'] = $kid;
		} else {
			$this->aceError('非法活动id');
		}
		$this->assign('kid', $kid);

		$count = $this->activeMeetingDao->countSearch($map);
		$Page = new \org\util\PageBootstrap($count, config('PER_PAGE'), $parameter);
		$pageShow = $Page->show();
		list($list, , $hids) = $this->activeMeetingDao->search($map, $Page->getLimit());
		//print_r($list);exit;
		$this->assign('count', $count);
		$this->assign('pageShow', $pageShow);
		$this->assign('list', $list);

		return $this->fetch();
	}

	public function listorders() {
		$result = parent::listorders($this->activeMeetingDao);

		if ($result !== false) {
			$this->success('排序更新成功');
		} else {
			$this->error('排序更新失败');
		}
	}

	public function add() {
		$id = input('param.id', '', '', 'intval');

		if ($id) {
			$info = $this->activeMeetingDao->getInfo($id);
			$kid = $info['kid'];
		} else {
			$info = [
				'status' => 1
			];
			$kid = input('param.kid', '', '', 'intval');
		}
		$this->assign('id', $id);
		$this->assign('info', $info);
		$this->assign('kid', $kid);

		return $this->fetch();
	}

	public function doAdd() {
		$id = input('post.id', '', '', 'intval');
		$data = input('post.data/a', '', '', 'pwEscape');

		if ($id) {
			$info = $this->activeMeetingDao->getInfo($id);
			$kid = $info['kid'];
		} else {
			$kid = input('post.kid', '', '', 'intval');
		}

		if ($id) {
			$result = $this->activeMeetingDao->baseUpdateData($id, $data);
			if ($result !== false) {
				$this->success("保存成功", url('actstar_manage/meeting/index', ['kid'=>$kid]));
			} else {
				$this->error('保存失败'.showDbError($this->activeMeetingDao));
			}
		} else {
			$data['kid'] = $kid;
			$result = $this->activeMeetingDao->baseAddData($data);
			if ($result !== false) {
				$this->success("添加成功", url('actstar_manage/meeting/index', ['kid'=>$kid]));
			} else {
				$this->error("添加失败".showDbError($this->activeMeetingDao));
			}
		}
	}

	public function doDelete() {
		$id = input('param.id', '', '', 'intval');

		//获取场次信息
		$meetingInfo = $this->activeMeetingDao->getInfo($id);
		$kid = $meetingInfo['kid'];

		$result = $this->activeMeetingDao->del($id);
		if ($result !== false) {
			$this->success('删除成功', url('actstar_manage/meeting/index', ['kid'=>$kid]));
		} else {
			$this->error('删除成功'.showDbError($this->activeMeetingDao));
		}
	}

	//修改库存
	public function stockEdit() {
		$id = input('param.id', '', '', 'intval');

		if ($id) {
			$info = $this->activeMeetingDao->getInfo($id);
			$this->assign('id', $id);
			$this->assign('info', $info);
		}

		echo $this->fetch()->getContent();
	}

	public function stockDoEdit() {
		$id = input('post.id', '', '', 'intval');
		$data = input('post.data/a', '', '', 'pwEscape');

		$meetingInfo = $this->activeMeetingDao->getInfo($id);
		if (!$meetingInfo) {
			$this->error('演出信息非法');
		}
		$kid = $meetingInfo['kid'];

		$result = $this->activeMeetingDao->baseUpdateData($id, $data);
		if ($result !== false) {
			$this->success('保存成功', url('actstar_manage/meeting/index', ['kid'=>$kid]));
		} else {
			$this->error('保存失败'.showDbError($this->activeMeetingDao));
		}
	}

}