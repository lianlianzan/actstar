<?php
/**
 * @Author: lianlianzan(13040@qq.com)
 * @Date:   2018-04-13 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-22 22:25:20
 */

namespace app\actstar_manage\controller;
use app\common\controller\ManageBase;
use think\facade\Config as thinkConfig;

class Wapcustom extends Pagedestination {

	function initialize() {
		parent::initialize();
		$this->saveAndGetConfigIdentify(model('Config'), '自定义模板', '', 'wapcustom');

		$this->wapcustomPieceDao = model('WapcustomPiece');
		$this->wapcustomInfopushDao = model('WapcustomInfopush');

		$this->assign('wapcustom_kind_config', config('wapcustom_kind_config')); //模块类别
		$this->assign('wapcustom_push_config', config('wapcustom_push_config')); //推送方式
		$this->assign('wapcustom_source_config', config('wapcustom_source_config')); //数据来源
		$wapcustom_source_flag = array();
		foreach (config('wapcustom_source_config') as $key => $value) {
			$wapcustom_source_flag[$value['flag']] = $value;
		}
		thinkConfig::set(['wapcustom_source_flag'=>$wapcustom_source_flag]); //添加系统配置
		$this->assign('page_type_config', config('page_type_config')); //目标页面类型
	}

	public function index() {
		//获取模块列表
		list($pieceList) = $this->wapcustomPieceDao->search([], '');
		$this->assign('pieceList', $pieceList);

		return $this->fetch();
	}

	public function listordersForNestable() {
		$vieworders = input('param.vieworders', '', '', 'pwEscape');
		$vieworders = json_decode($vieworders, true);

		if ($vieworders) {
			foreach ($vieworders as $key => $value) {
				$data = [];
				$data['vieworder'] = $key;
				$result = $this->wapcustomPieceDao->baseUpdateData($value['id'], $data);
			}
		}

		if ($result !== false) {
			$this->success('排序更新成功', '');
		} else {
			$this->error('排序更新失败', '');
		}
	}

	public function pieceAdd() {
		//获取模块列表
		list($pieceList, $pieceIds) = $this->wapcustomPieceDao->search([], '');
		$this->assign('pieceList', $pieceList);

		$pieceId = input('param.pieceId', '', '', 'intval');
		if ($pieceId) {
			$pieceInfo = $this->wapcustomPieceDao->getInfo($pieceId);
			$kindId = $pieceInfo['kind_id'];
		} else {
			$kindId = input('param.kindId', '', '', 'intval');
			if (!$kindId) {
				$this->pcError('非法模块种类ID');
			}
			$pieceId = 0;
			$pieceInfo = [
				'status'		=> 1,
				'name' 			=> $wapcustom_kind_config[$kindId]['name'],
				'flag' 			=> $wapcustom_kind_config[$kindId]['flag'],
				'data_source'	=> 0,
			];
		}
		$this->assign('pieceId', $pieceId);
		$this->assign('pieceInfo', $pieceInfo);
		
		$wapcustom_kind_config = config('wapcustom_kind_config'); //模板类别
		$kindFlag = $wapcustom_kind_config[$kindId]['flag'];
		$this->assign('kindId', $kindId);
		$this->assign('kindFlag', $kindFlag);

		return $this->fetch();
	}

	public function pieceDoAdd() {
		$wapcustom_kind_config = config('wapcustom_kind_config'); //模板类别

		$pieceId = input('post.pieceId', '', '', 'intval');
		$data = input('post.data/a', '', '', 'pwEscape');
		$status = input('post.status', '', '', 'intval');
		if ($status) {
			$data['status'] = 1;
		} else {
			$data['status'] = 0;
		}

		if ($pieceId) {
			$info = $this->wapcustomPieceDao->get($pieceId);
		} else {
			$kindId = input('param.kindId', '', '', 'intval');
			if (!$kindId) {
				$this->error('非法模块类别ID');
			}
			$data['kind_id'] = $kindId;
			$moduleInfo = $wapcustom_kind_config[$kindId];
			if ($kindId == 1) {
				$data['name'] = $moduleInfo['name'];
			}
		}

		$data = $this->pieceForSource($data);
		if ($data['push_mode'] == 2) {
			$data['data_source'] = '';
		}
		
		//普通上传图片
		$uploadInfo = $this->comuploadFile('actstar/wapcustom', 'ad_picurl', $info['ad_picurl']);
		$uploadInfo && $data['ad_picurl'] = $uploadInfo['saveFullPath'];
		
		if ($pieceId) {
			$result = $this->wapcustomPieceDao->baseUpdateData($pieceId, $data);
			if ($result !== false) {
				$this->success('保存成功', '', ['mode'=>'jumplink', 'okLink'=>url('actstar_manage/wapcustom/pieceAdd', ['pieceId'=>$pieceId])]);
			} else {
				$this->error('保存失败'.showDbError($this->wapcustomPieceDao));
			}
		} else {
			$data['create_time'] = $this->ts;
			$result = $pieceId = $this->wapcustomPieceDao->baseAddData($data);
			if ($result !== false) {
				$this->success('添加成功', '', ['mode'=>'jumplink', 'okLink'=>url('actstar_manage/wapcustom/pieceAdd', ['pieceId'=>$pieceId])]);
			} else {
				$this->error('添加失败'.showDbError($this->wapcustomPieceDao));
			}
		}
	}

	public function pieceDoDelete() {
		$pieceId = input('param.pieceId', '', '', 'intval');
		if (!$pieceId) {
			$this->error('非法ID');
		}
		//获取详情
		$info = $this->wapcustomPieceDao->getInfo($pieceId);

		$result = $this->wapcustomPieceDao->delInfo($pieceId);

		//删除文件
		//$info['picurl'] && $this->delFtpFile($info['picurl']);

		if ($result !== false) {
			$this->success('删除成功');
		} else {
			$this->error('删除成功'.showDbError($this->wapcustomPieceDao));
		}
	}

	public function infopushList() {
		//获取模块列表
		list($pieceList, $pieceIds) = $this->wapcustomPieceDao->search([], '');
		$this->assign('pieceList', $pieceList);

		$wapcustom_kind_config = config('wapcustom_kind_config'); //模块类别
		$pieceId = input('param.pieceId', '', '', 'intval');
		if ($pieceId) {
			$pieceInfo = $this->wapcustomPieceDao->getInfo($pieceId);
			$kindId = $pieceInfo['kind_id'];
		} else {
			$kindId = input('param.kindId', '', '', 'intval');
			if (!$kindId) {
				$this->pcError('非法模块类别ID');
			}
			$pieceInfo = [
				'name' => $wapcustom_kind_config[$kindId]['name'],
				'flag' => $wapcustom_kind_config[$kindId]['flag']
			];
		}
		$this->assign('pieceId', $pieceId);
		$this->assign('pieceInfo', $pieceInfo);

		$moduleFlag = $wapcustom_kind_config[$kindId]['flag'];
		$this->assign('kindId', $kindId);
		$this->assign('moduleFlag', $moduleFlag);

		//获取推送信息列表
		$map = [
			'piece_id' => $pieceId
		];
		list($infopushList) = $this->wapcustomInfopushDao->search($map, '');
		$this->assign('infopushList', $infopushList);

		return $this->fetch();
	}

	public function infopushListorders() {
		$result = parent::listordersOrigin($this->wapcustomInfopushDao);
		if ($result !== false) {
			$this->success('排序更新成功');
		} else {
			$this->error('排序更新失败');
		}
	}

	public function infopushAdd() {
		$id = input('param.id', '', '', 'intval');
		if ($id) {
			$info = $this->wapcustomInfopushDao->getInfo($id);
			$pieceId = $info['piece_id'];
		} else {
			$info = [
				'status' => 1
			];
			$pieceId = input('param.pieceId', '', '', 'intval');
		}
		$this->assign('id', $id);
		$this->assign('info', $info);

		//获取模块列表
		list($pieceList, $pieceIds) = $this->wapcustomPieceDao->search([], '');
		$this->assign('pieceList', $pieceList);

		$wapcustom_kind_config = config('wapcustom_kind_config'); //模块类别
		if ($pieceId) {
			$pieceInfo = $pieceList[$pieceId];
			$kindId = $pieceInfo['kind_id'];
		} else {
			$kindId = input('param.kindId', '', '', 'intval');
			if (!$kindId) {
				$this->error('非法模块类别ID');
			}
			$info = [
				'name' => $wapcustom_kind_config[$kindId]['name'],
				'flag' => $wapcustom_kind_config[$kindId]['flag']
			];
		}
		$this->assign('pieceId', $pieceId);
		$this->assign('pieceInfo', $pieceInfo);

		return $this->fetch();
	}

	public function infopushDoAdd() {
		$pieceId = input('post.pieceId', '', '', 'intval');
		$id = input('post.id', '', '', 'intval');
		$data = input('post.data/a', '', '', 'pwEscape');

		if ($id) {
			$info = $this->wapcustomInfopushDao->get($id);
		}

		//普通上传图片
		$uploadInfo = $this->comuploadFile('actstar/wapcustom', 'picurl', $info['picurl']);
		$uploadInfo && $data['picurl'] = $uploadInfo['saveFullPath'];

		if ($id) {
			$result = $this->wapcustomInfopushDao->baseUpdateData($id, $data);
			if ($result !== false) {
				$this->success('保存成功', url('actstar_manage/wapcustom/infopushList', ['pieceId'=>$pieceId]));
			} else {
				$this->error('保存失败'.showDbError($this->wapcustomPieceDao));
			}
		} else {
			$data['piece_id'] = $pieceId;
			$data['create_time'] = $this->ts;
			$result = $this->wapcustomInfopushDao->baseAddData($data);
			if ($result !== false) {
				$this->success('添加成功', url('actstar_manage/wapcustom/infopushList', ['pieceId'=>$pieceId]));
			} else {
				$this->error('添加失败'.showDbError($this->wapcustomPieceDao));
			}
		}
	}

	public function infopushDoDelete() {
		$id = input('param.id', '', '', 'intval');

		//获取信息(不解析)
		$info = $this->wapcustomInfopushDao->get($id);
		$pieceId = $info['piece_id'];

		//删除对应数据库信息
		$result = $this->wapcustomInfopushDao->delInfo($id);

		//删除图片文件
		$info['picurl'] && $this->delFtpFile($info['picurl']);

		if ($result !== false) {
			$this->success('删除成功', url('actstar_manage/wapcustom/infopushList', ['pieceId'=>$pieceId]));
		} else {
			$this->error('删除成功'.showDbError($this->wapcustomInfopushDao));
		}
	}

}