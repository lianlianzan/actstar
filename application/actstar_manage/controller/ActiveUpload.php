<?php
/**
 * @Author: lianlianzan(13040@qq.com)
 * @Date:   2018-08-01 16:40:00
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-17 15:24:24
 */

namespace app\actstar_manage\controller;
use app\common\controller\ManageBase;

class ActiveUpload extends ManageBase {

	 function initialize() {
		parent::initialize();

		$this->activeDao = model('Active');
		$this->activePictureDao = model('ActivePicture');
	}

	public function doUpload() {
		$fileName = input('post.fileName', '', '', 'pwEscape');
		$fileType = input('post.fileType', '', '', 'pwEscape');
		$fileSize = input('post.fileSize', '', '', 'pwEscape');
		$fileData = input('post.fileData', '', '', 'pwEscape');

		$h5Upload = new \org\util\H5Upload();
		$h5Upload->saveFtpPath = 'actstar/active/Mon_'.date('ym') . '/';
		$randname  = substr(md5(time() . randstr(8)),10,15);
		//$h5Upload->saveFtpName = '_'.$this->s_uid.'_'.$randname;
		$h5Upload->fileName = $fileName;
		$h5Upload->fileType = $fileType;
		$h5Upload->fileSize = $fileSize;
		$h5Upload->fileData = $fileData;
		$h5Upload->upload(); //开始上传
		if ($h5Upload->getError()) {
			$this->error($h5Upload->getError());
		}

		$uploadInfo = $h5Upload->getUploadInfo();

		if ($uploadInfo) {
			//数据库写入操作
			$data = [
				'filename'			=> $fileName,
				'picurl'			=> $uploadInfo['attachurl'],
				'filetype'			=> $uploadInfo['ext'],
				'filesize'			=> $uploadInfo['size'],
				'create_time'		=> $this->ts,
			];
			$id = $this->activePictureDao->baseAddData($data);
			//print_r($this->activePictureDao->getLastSql());exit;
			$resultData = array(
				'id'	=> $id,
			);
			$this->success('', '', $resultData);
		}
	}

	public function doDelete() {
		$id = input('post.id', '', '', 'intval');
		if (!$id) {
			$this->error('非法ID');
		}

		//获取信息(不解析)
		$info = $this->activePictureDao->get($id);
		if (!$info) {
			$this->error('该图片已经被删除');
		}

		//删除对应数据库信息
		$this->activePictureDao->delInfo($id);

		//删除图片文件
		$info['picurl'] && $this->delFtpFile($info['picurl']);

		//计算图片数
		$pictureCount = $this->activePictureDao->countSearch(['pid'=>$info['pid']]);
		$data = [
			'pic_num' => $pictureCount,
		];
		$this->activeDao->baseUpdateData($info['pid'], $data);

		$this->success('删除图片成功');
	}

}