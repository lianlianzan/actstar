<?php
/**
 * @Author: lianlianzan(13040@qq.com)
 * @Date:   2020-11-16 22:40:06
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-17 15:57:47
 */

namespace app\actstar_manage\controller;
use app\common\controller\ManageBase;

class ActiveCategory extends ManageBase {

	function initialize() {
		parent::initialize();
		$this->saveAndGetConfigIdentify(model('Config'), '分类管理', '', 'activeCategory');

		$this->activeCategoryDao = model('ActiveCategory');
	}

	public function index() {
		//获取文章分类列表
		list($categoryList) = $this->activeCategoryDao->search([], '');
		$this->assign('categoryList', $categoryList);

		return $this->fetch();
	}

	public function listorders() {
		$result = parent::listordersOrigin($this->activeCategoryDao);
		if ($result !== false) {
			$this->success('排序更新成功');
		} else {
			$this->error('排序更新失败');
		}
	}

	public function add() {
		$id = input('param.id', '', '', 'intval');

		if ($id) {
			$info = $this->activeCategoryDao->getInfo($id);
		} else {
			$info = [
				'status' => 1
			];
		}
		$this->assign('id', $id);
		$this->assign('info', $info);

		return $this->fetch();
	}

	public function doAdd() {
		$id = input('post.id', '', '', 'intval');
		$data = input('post.data/a', '', '', 'pwEscape');

		if ($id) {
			$info = $this->activeCategoryDao->get($id);
		}

		//普通上传图片
		$uploadInfo = $this->comuploadFile('actstar/active_category', 'picurl', $info['picurl']);
		$uploadInfo && $data['picurl'] = $uploadInfo['saveFullPath'];

		//验证数据
		$validate = validate('ArticleCategory');
		if (!$validate->check($data)) {
			$this->error($validate->getError());
		}

		if ($id) {
			$result = $this->activeCategoryDao->baseUpdateData($id, $data);
			$this->clearCache(); //清除缓存
			if ($result !== false) {
				$this->success('保存成功', url('actstar_manage/articleCategory/index'));
			} else {
				$this->error('保存失败'.showDbError($this->activeCategoryDao));
			}
		} else {
			$result = $this->activeCategoryDao->baseAddData($data);
			$this->clearCache(); //清除缓存
			if ($result !== false) {
				$this->success('添加成功', url('actstar_manage/articleCategory/index'));
			} else {
				$this->error('添加失败'.showDbError($this->activeCategoryDao));
			}
		}
	}

	public function doDelete() {
		$id = input('param.id', '', '', 'intval');

		$result = $this->activeCategoryDao->del($id);
		$this->clearCache(); //清除缓存
		if ($result !== false) {
			$this->success('删除成功', url('actstar_manage/articleCategory/index'));
		} else {
			$this->error('删除失败'.showDbError($this->activeCategoryDao));
		}
	}

	private function clearCache() {
		$sys_identify = 'active_category';
		cache($sys_identify, NULL); //删除缓存数据
	}

}