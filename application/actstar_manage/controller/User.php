<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2019-01-10 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-17 13:02:33
 */

namespace app\actstar_manage\controller;
use app\common\controller\ManageBase;

class User extends ManageBase {

	protected $userDao;

	function initialize() {
		parent::initialize();
		$this->saveAndGetConfigIdentify(model('Config'), '用户管理', '', 'user');

		$this->userDao = model('User');
	}

	public function index() {
		list($map, $parameter) = $this->getMap();

		$count = $this->userDao->countSearch($map);
		$Page = new \org\util\PageBootstrap($count, config('PER_PAGE'), $parameter);
		$pageShow = $Page->show();
		list($list) = $this->userDao->search($map, $Page->getLimit(), $orderby);
		$this->assign("count", $count);
		$this->assign('pageShow', $pageShow);
		$this->assign("list", $list);

		return $this->fetch();
	}

	private function getMap() {
		$map = $parameter = array();

		$status = input('param.status', '', '', 'pwEscape');
		if ($status) {
			$map['status'] = $status;
			$parameter['status'] = $status;
		}
		$this->assign('status', $status);

		$uid = input('param.uid', '', '', 'pwEscape');
		if ($uid) {
			$map['uid'] = $uid;
			$parameter['uid'] = $uid;
		}
		$this->assign('uid', $uid);

		$username = input('param.username', '', '', 'pwEscape');
		if ($username) {
			$map['username'] = array('like', '%'.$username.'%');
			$parameter['username'] = $username;
		}
		$this->assign('username', $username);

		$name = input('param.name', '', '', 'pwEscape');
		if ($name) {
			$map['name'] = array('like', '%'.$name.'%');
			$parameter['name'] = $name;
		}
		$this->assign('name', $name);

		return array($map, $parameter);
	}

	public function add() {
		$id = input('param.id', '', '', 'intval');

		if ($id) {
			$info = $this->userDao->getInfo($id);
		}

		$this->assign('info', $info);

		return $this->fetch();
	}

	public function doAdd() {
		$id = input('post.id', '', '', 'intval');
		$data = input('post.data/a', '', '', 'pwEscape');

		if ($id) {
			$info = $this->userDao->get($id);
		}

		if ($id) {
			$result = $this->userDao->baseUpdateData($id, $data);
			if ($result === false) {
				$this->error('保存失败'.showDbError($this->userDao));
			}
		} else {
			$data['status'] = 1;
			$data['create_time'] = $this->ts;
			$result = $id = $this->userDao->baseAddData($data);
			if ($result === false) {
				$this->error('添加失败'.showDbError($this->userDao));
			}
		}

		if ($id) {
			$this->success('保存成功', url('actstar_manage/user/index'));
		} else {
			$this->success('添加成功', url('actstar_manage/user/index'));
		}
	}

	public function doDelete() {
		$id = input('param.id', '', '', 'intval');

		$result = $this->userDao->del($id);
		if ($result !== false) {
			$this->success('删除成功', url('actstar_manage/user/index'));
		} else {
			$this->error('删除失败'.showDbError($this->userDao));
		}
	}

	/**
	 * 设置核销员
	 */
	public function doSetWriteoff() {
		$id = input('param.id', '', '', 'intval');

		//获取用户信息
		$info = $this->userDao->get($id);

		$data = [
			'is_writeoff'			=> 1,
		];
		$result = $this->userDao->baseUpdateData($id, $data);

		if ($result !== false) {
			$this->success("设置核销员成功", '');
		} else {
			$this->error("设置核销员失败".showDbError($this->userDao));
		}
	}

	/**
	 * 取消核销员
	 */
	public function doCancelWriteoff() {
		$id = input('param.id', '', '', 'intval');

		//获取用户信息
		$info = $this->userDao->get($id);

		$data = [
			'is_writeoff'			=> 0,
		];
		$result = $this->userDao->baseUpdateData($id, $data);

		if ($result !== false) {
			$this->success("取消核销员成功", '');
		} else {
			$this->error("取消核销员失败".showDbError($this->userDao));
		}
	}

}