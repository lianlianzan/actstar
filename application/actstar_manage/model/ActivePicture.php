<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2018-04-13 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-17 15:22:29
 */

namespace app\actstar_manage\model;
use app\common\model\BaseNew;

class ActivePicture extends BaseNew {
	// 设置数据表（不含前缀）
	protected $name = 'as_active_picture';

	// 设置当前模型的数据库连接
	protected $connection = 'db_kszhuangxiu_pieceapp_config';

	// 定义时间戳字段名
	protected $createTime = '';
	protected $updateTime = '';

	public function getListByKid($kid) {
		$map = array(
			'kid'	=> $kid,
		);
		$data = $this->where($map)->order('vieworder asc')->select();
		$data = $data->toArray(); //转换为数组
		return $this->parseSearch($data);
	}

	public function getUnUseAttach() {
		$map = array(
			'kid'	=> 0,
		);
		$data = $this->where($map)->order('vieworder asc')->select();
		$data = $data->toArray(); //转换为数组
		return $this->parseSearch($data);
	}

	public function countSearch($map) {
		return $this->where($map)->count();
	}

	public function search($map, $limit) {
		$data = $this->where($map)->order('create_time asc')->limit($limit)->select();
		$data = $data->toArray(); //转换为数组
		//print_r($this->getLastSql());exit;
		return $this->parseSearch($data);
	}

	protected function parseSearch($data) {
		$ftpWeb = config('app.ftp_web');
		$isopen_config = config('extend.isopen_config');

		$list = $ids = array();
		foreach ($data as $key => $value) {
			//通用解析
			//$value['picurl'] = $value['picurl'] ? $ftpWeb.$value['picurl'] : '';
			//$value['create_time_show'] = $value['create_time'] ? date("Y-m-d H:i:s", $value['create_time']) : '';
			//$value['status_show'] = isset($value['status']) ? $isopen_config[$value['status']] : '';
			//通用解析

			$value['picurl'] = $value['picurl'] ? $ftpWeb.$value['picurl'] : '';

			$list[$value['id']] = $value;
			$ids[$value['id']] = $value['id'];
		}
		return array($list, $ids);
	}

}