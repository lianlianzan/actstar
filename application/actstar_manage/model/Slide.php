<?php
/**
 * @Author: lianlianzan(13040@qq.com)
 * @Date:   2018-09-11 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-19 15:16:18
 */

namespace app\actstar_manage\model;
use app\common\model\BaseNew;

class Slide extends BaseNew {
	// 设置数据表（不含前缀）
	protected $name = 'as_slide';

	// 设置当前模型的数据库连接
	protected $connection = 'db_kszhuangxiu_pieceapp_config';

	// 定义时间戳字段名
	protected $createTime = '';
	protected $updateTime = '';

	// 定义排序字段
	protected $_orderField = 'vieworder';
	protected $_orderDesc = 'desc';

	public function countSearch($map) {
		return $this->where($map)->count();
	}

	public function search($map, $limit='', $orderBy='') {
		if (!$orderBy) {
			$orderBy = array($this->_orderField=>$this->_orderDesc);
		}
		$data = $this->where($map)->order($orderBy)->limit($limit)->select();
		$data = $data->toArray(); //转换为数组
		//print_r($this->getLastSql());exit;
		return $this->parseSearch($data);
	}

	protected function parseSearch($data) {
		$ftpWeb = config('app.ftp_web');
		$isopen_config = config('extend.isopen_config');

		$list = $ids = array();
		foreach ($data as $key => $value) {
			//通用解析
			//$value['create_time_show'] = $value['create_time'] ? date("Y-m-d H:i:s", $value['create_time']) : '';
			//$value['status_show'] = isset($value['status']) ? $isopen_config[$value['status']] : '';
			//$value['picurl'] = $value['picurl'] ? $ftpWeb.$value['picurl'] : '';
			//通用解析

			$value['create_time_show'] = $value['create_time'] ? date("Y-m-d H:i:s", $value['create_time']) : '';
			$value['status_show'] = isset($value['status']) ? $isopen_config[$value['status']] : '';
			$value['picurl'] = $value['picurl'] ? $ftpWeb.$value['picurl'] : '';

			//组装链接
			$value['link'] = dealLinkTwo($value['page_flag'], $value['page_data'], $value['page_extra']);

			$tmpArr = array();
			// $tmpArr['id'] = $value['id'];
			// $tmpArr['title'] = $value['title'];
			// $tmpArr['picurl'] = $value['picurl'];
			// $tmpArr['page_flag'] = $value['page_flag'];
			// $tmpArr['page_data'] = $value['page_data'];
			$listApi[] = $tmpArr;

			$list[$value['id']] = $value;
			$ids[$value['id']] = $value['id'];
		}
		return array($list, $ids);
	}

	protected function parseInfo($info) {
		$ftpWeb = config('app.ftp_web');
		$isopen_config = config('extend.isopen_config');

		//通用解析
		//$info['create_time_show'] = $info['create_time'] ? date("Y-m-d H:i:s", $info['create_time']) : '';
		//$info['status_show'] = isset($info['status']) ? $isopen_config[$info['status']] : '';
		//$info['picurl'] = $info['picurl'] ? $ftpWeb.$info['picurl'] : '';
		//通用解析

		$info['picurl'] = $info['picurl'] ? $ftpWeb.$info['picurl'] : '';
		$info['create_time'] = $info['create_time'] ? date("Y-m-d H:i:s", $info['create_time']) : '';

		return $info;
	}

}