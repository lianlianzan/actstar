<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2018-07-11 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-24 16:57:43
 */

namespace app\actstar_manage\model;
use app\common\model\BaseNew;

class ActiveMeeting extends BaseNew {
	// 设置数据表（不含前缀）
	protected $name = 'as_active_meeting';

	// 设置当前模型的数据库连接
	protected $connection = 'db_kszhuangxiu_pieceapp_config';

	// 定义时间戳字段名
	protected $createTime = '';
	protected $updateTime = '';

	public function getGroupByPids($ids) {
		$map = array(
			'pid'	=> array('in', implode(',', $ids)),
		);
		$data = $this->where($map)->select();
		$data = $data->toArray(); //转换为数组
		//print_r($this->getLastSql());
		return $this->parseGroup($data);
	}

	private function parseGroup($data) {
		$ftpWeb = config('app.ftp_web');
		$list = $oids = array();
		foreach ($data as $key => $value) {
			//$value['attachurl_show'] = $ftpWeb.$value['attachurl'];
			$list[$value['pid']][$value['id']] = $value;
		}
		return $list;
	}
	public function getSpecGroupByPid($pid) {
		$map = array(
			'product_id'	=> $pid,
			'vieworder'		=> array('gt', 0),
		);
		$data = $this->where($map)->order('vieworder desc')->select();
		$data = $data->toArray(); //转换为数组
		//print_r($this->getLastSql());
		return $this->parseSpecGroup($data);
	}

	private function parseSpecGroup($data) {
		$list = array();
		$ftpWeb = config('app.ftp_web');
		foreach ($data as $key => $value) {
			//$value['attachurl_show'] = $ftpWeb.$value['attachurl'];
			$list[$value['spec_1']][$value['spec_2']] = $value;
		}

		//print_r($list);
		return array($list);
	}

	public function getListByKid($kid) {
		$map = array(
			'kid'		=> $kid,
			'status'	=> 1
		);
		$data = $this->where($map)->order('vieworder desc')->select();
		$data = $data->toArray(); //转换为数组
		//print_r($this->getLastSql());
		return $this->parseSearch($data);
	}

	public function countSearch($map) {
		return $this->where($map)->count();
	}

	public function search($map, $limit) {
		$data = $this->where($map)->order('vieworder desc')->limit($limit)->select();
		$data = $data->toArray(); //转换为数组
		//print_r($this->getLastSql());exit;
		return $this->parseSearch($data);
	}

	protected function parseSearch($data) {
		$ftpWeb = config('app.ftp_web');
		$isopen_config = config('extend.isopen_config');

		$list = $ids = array();
		foreach ($data as $key => $value) {
			//通用解析
			//$value['create_time_show'] = $value['create_time'] ? date("Y-m-d H:i:s", $value['create_time']) : '';
			//$value['status_show'] = isset($value['status']) ? $isopen_config[$value['status']] : '';
			//$value['picurl'] = $value['picurl'] ? $ftpWeb.$value['picurl'] : '';
			//通用解析

			$value['status_show'] = isset($value['status']) ? $isopen_config[$value['status']] : '';

			if ($value['stock_num']) {
				$value['stock_show'] = '(限制'.$value['stock_num'].'人报名)';
			} else {
				$value['stock_show'] = '';
			}
			$list[$value['id']] = $value;
			$ids[$value['id']] = $value['id'];
		}
		return array($list, $ids, $hids);
	}

	protected function parseInfo($info) {
		$ftpWeb = config('app.ftp_web');
		$isopen_config = config('extend.isopen_config');

		return $info;
	}

}