<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2018-04-13 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-17 15:06:07
 */

namespace app\actstar_manage\model;
use app\common\model\BaseNew;

class Config extends BaseNew {
	// 设置数据表（不含前缀）
	protected $name = 'as_config';

	// 设置当前模型的数据库连接
	protected $connection = 'db_kszhuangxiu_pieceapp_config';

	// 定义时间戳字段名
	protected $createTime = '';
	protected $updateTime = '';

	public function getConfigsBySpace($config_space) {
		$data = $this->where(array('config_space'=>$config_space))->select();
		$data = $data->toArray(); //转换为数组
		$list = array();
		foreach ($data as $key => $value) {
			$list[$value['config_name']] = $value['config_value'];
		}
		//支付方式
		$list['is_alipay'] = $list['allow_pay_method'] & 1 ? 1 : 0;
		$list['is_wxpay'] = $list['allow_pay_method'] & 2 ? 1 : 0;
		return $list;
	}

	public function getConfigByNameAndSpace($config_name, $config_space) {
		$info = $this->where(array('config_name'=>$config_name, 'config_space'=>$config_space))->find();
		return $info;
	}

	public function getConfigValueByNameAndSpace($config_name, $config_space) {
		$info = $this->where(array('config_name'=>$config_name, 'config_space'=>$config_space))->find();
		return $info['config_value'];
	}

	public function updateConfigByNameAndSpace($config_name, $config_space, $data) {
		$result = $this->where(array('config_name'=>$config_name, 'config_space'=>$config_space))->update($data);
		return $result;
	}

	protected function parseInfo($info) {
		$ftpWeb = config('app.ftp_web');

		$info['create_time'] = date("Y-m-d H:i:s", $info['create_time']);
		$info['pic'] = $info['pic'] ? $ftpWeb.$info['pic'] : '';

		return $info;
	}

}