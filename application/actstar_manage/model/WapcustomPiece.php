<?php
/**
 * @Author: lianlianzan(13040@qq.com)
 * @Date:   2018-04-13 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-23 13:07:16
 */

namespace app\actstar_manage\model;
use app\common\model\BaseNew;

class WapcustomPiece extends BaseNew {
	// 设置数据表（不含前缀）
	protected $name = 'as_wapcustom_piece';

	// 设置当前模型的数据库连接
	protected $connection = 'db_kszhuangxiu_pieceapp_config';

	// 定义时间戳字段名
	protected $createTime = '';
	protected $updateTime = '';

	public function countSearch($map) {
		return $this->where($map)->count();
	}

	public function search($map, $limit) {
		$data = $this->where($map)->order('vieworder asc')->limit($limit)->select();
		$data = $data->toArray(); //转换为数组
		//print_r($this->getLastSql());
		return $this->parseSearch($data);
	}

	protected function parseSearch($data) {
		$ftpWeb = config('app.ftp_web');
		$isopen_config = config('extend.isopen_config');
		$wapcustom_source_flag = config('wapcustom_source_flag');

		$list = $ids = array();
		foreach ($data as $key => $value) {
			//通用解析
			//$value['create_time_show'] = $value['create_time'] ? date("Y-m-d H:i:s", $value['create_time']) : '';
			//$value['status_show'] = isset($value['status']) ? $isopen_config[$value['status']] : '';
			//$value['picurl'] = $value['picurl'] ? $ftpWeb.$value['picurl'] : '';
			//通用解析

			$value['more_name'] = $value['more_name'] ? $value['more_name'] : '更多';
			$value['push_mode_show'] = $value['push_mode'] == 1 ? '自动' : '手动';

			$value['data_show'] = '';
			if ($value['data_source']) {
				$value['data_show'] .= $wapcustom_source_flag[$value['data_source']]['name'];
			}
			if ($value['cate_name']) {
				$value['data_show'] .= ','.$value['cate_name'];
			}
			if ($value['sort_name']) {
				$value['data_show'] .= ','.$value['sort_name'];
			}

			$value['ad_picurl'] = $value['ad_picurl'] ? $ftpWeb.$value['ad_picurl'] : '';
			$value['ad_picurl2'] = $value['ad_picurl2'] ? $ftpWeb.$value['ad_picurl2'] : '';
			$value['ad_picurl3'] = $value['ad_picurl3'] ? $ftpWeb.$value['ad_picurl3'] : '';
			$value['is_slide'] = 0;
			if ($value['ad_picurl2']) {
				$value['is_slide'] = 1;
			}

			//组装链接
			$value['more_link'] = dealLink($value['more_page_flag'], $value['more_page_data']);
			$value['ad_link'] = dealLink($value['ad_page_flag'], $value['ad_page_data']);

			$list[$value['id']] = $value;
			$ids[$value['id']] = $value['id'];
		}
		return array($list, $ids);
	}

	protected function parseInfo($info) {
		$ftpWeb = config('app.ftp_web');

		//通用解析
		//$info['create_time_show'] = $info['create_time'] ? date("Y-m-d H:i:s", $info['create_time']) : '';
		//$info['status_show'] = isset($info['status']) ? $isopen_config[$info['status']] : '';
		//$info['picurl'] = $info['picurl'] ? $ftpWeb.$info['picurl'] : '';
		//通用解析

		$info['ad_picurl'] = $info['ad_picurl'] ? $ftpWeb.$info['ad_picurl'] : '';
		$info['ad_picurl2'] = $info['ad_picurl2'] ? $ftpWeb.$info['ad_picurl2'] : '';
		$info['ad_picurl3'] = $info['ad_picurl3'] ? $ftpWeb.$info['ad_picurl3'] : '';
		$info['is_slide'] = 0;
		if ($info['ad_picurl2']) {
			$info['is_slide'] = 1;
		}

		return $info;
	}

}