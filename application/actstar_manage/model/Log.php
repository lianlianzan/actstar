<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2017-09-27 16:40:00
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-20 10:25:00
 */

namespace app\actstar_manage\model;
use app\common\model\BaseNew;

class Log extends BaseNew {
	// 设置数据表（不含前缀）
	protected $name = 'as_log';

	// 设置当前模型的数据库连接
	protected $connection = 'db_kszhuangxiu_pieceapp_config';

	// 定义时间戳字段名
	protected $createTime = '';
	protected $updateTime = '';

	protected $_orderField = 'create_time';
	protected $_orderDesc = 'desc';

	public function countSearch($map) {
		$count = $this->where($map)->count();
		return $count;
	}

	public function search($map, $limit, $orderBy='') {
		if (!$orderBy) {
			$orderBy = array($this->_orderField=>$this->_orderDesc);
		}
		$data = $this->where($map)->order($orderBy)->limit($limit)->select();
		$data = $data->toArray(); //转换为数组
		//print_r($this->getLastSql());
		return $this->parseSearch($data);
	}

	protected function parseSearch($data) {
		$ftpWeb = config('app.ftp_web');
		$isopen_config = config('extend.isopen_config');

		$list = $uids = array();
		foreach ($data as $key => $value) {
			//通用解析
			//$value['create_time_show'] = $value['create_time'] ? date("Y-m-d H:i:s", $value['create_time']) : '';
			//$value['status_show'] = isset($value['status']) ? $isopen_config[$value['status']] : '';
			//$value['picurl'] = $value['picurl'] ? $ftpWeb.$value['picurl'] : '';
			//通用解析

			$value['create_time_show'] = $value['create_time'] ? date("Y-m-d H:i:s", $value['create_time']) : '';

			$list[$value['id']] = $value;
			$uids[$value['uid']] = $value['uid'];
		}
		return array($list, $uids);
	}

	protected function parseInfo($info) {
		$ftpWeb = config('app.ftp_web');
		$isopen_config = config('extend.isopen_config');

		//通用解析
		//$info['create_time_show'] = $info['create_time'] ? date("Y-m-d H:i:s", $info['create_time']) : '';
		//$info['status_show'] = isset($info['status']) ? $isopen_config[$info['status']] : '';
		//$info['picurl'] = $info['picurl'] ? $ftpWeb.$info['picurl'] : '';
		//通用解析

		return $info;
	}

}