<?php
/**
 * @Author: lianlianzan(13040@qq.com)
 * @Date:   2018-07-11 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-17 11:15:24
 * 规则验证器
 */

namespace app\actstar_manage\validate;
use think\Validate;

class ArticleCategory extends Validate {
    // 验证规则
    protected $rule = [
        'name' => 'require',
    ];

    protected $message = [
        'name.require' => '名称不能为空',
    ];

}