<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2018-07-11 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-17 15:33:05
 * 规则验证器
 */

namespace app\actstar_manage\validate;
use think\Validate;

class Article extends Validate {
    // 验证规则
    protected $rule = [
        'title' => 'require',
    ];

    protected $message = [
        'title.require' => '文章标题不能为空',
    ];

    //protected $scene=[
    //    'edit' => ['title','name','depend_type','depend_flag'],
    //];
}